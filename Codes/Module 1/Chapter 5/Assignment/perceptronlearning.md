# **Perceptron learning**
## Objective
The objective of this experiment is to illustrate the concept of perceptron learning in the context of pattern classification task. Following are the goals of the experiment:

-   To demonstrate the perceptron learning law.
    
-   To illustrate the convergence of the weights for linearly separable classes.
    
-   To observe the behaviour of the neural network for two classes which are not linearly separable.

## Illustration
### Illustration of perceptron learning for two-dimensional input

Consider two linearly separable classes, where each class consists of two-dimensional input pattern vectors. An example of two-class classification problem is shown below. The input vectors belonging to each class are shown in different colours in the following figure. The line is defined by the equation \( w_1 x + w_2 y = \theta.\) The straight line is defined by the initial values of the weights and the threshold.- 
<br>
![](http://cse22-iiith.vlabs.ac.in/exp3/images/perceptron_2class_1.jpg)
<br>
**Figure 1**: *Initial weights and the classes to be separated.*

After the convergence of weights, the line separates the two classes. This is shown in the following figure.
<br>
![](http://cse22-iiith.vlabs.ac.in/exp3/images/perceptron_2class_2.jpg)
<br>
**Figure 2**: *After the weights have converged, the final values of weights determine the line separating the classes.*

### Convergence of weights and threshold value
The following figures show the variation of threshold \( \theta \), and the weights \( w_1 \) and \(w_2\), as functions of the iteration index. The convergence of \( \theta, w_1 \) and \( w_2 \) can be noted.
<br>
![](http://cse22-iiith.vlabs.ac.in/exp3/images/thetaVsTime.jpg)
<br>
**Figure 3**: *The variation in values of \(\theta\) with iterations as network reaches convergence.*
<br>
![](http://cse22-iiith.vlabs.ac.in/exp3/images/w2VsTime.jpg)
<br>
**Figure 4**: *The variation in values of weight \(w_1\) with iterations as network reaches convergence*.
<br>
![](http://cse22-iiith.vlabs.ac.in/exp3/images/w1VsTime.jpg)
<br>
**Figure 5**: *The variation in values of weight \(w_2\) with iterations as network reaches convergence.*
## Procedure
- Select a problem type for generating two classes as 'Linearly separable' or 'Linearly inseparable'.
- Choose for a number of samples per class and number of iterations the perceptron network must go through.
- Choose a step size to move over the number of iterations for display of results for the perceptron network.
- Intialize the perceptron network by clicking on the 'Init perceptron' button.
- Click on button for display of results according to the given step size.
## Experiment
### Illustration of perceptron learning
Click Here:http://cse22-iiith.vlabs.ac.in/exp3/Experiment.html?domain=Computer%20Science&lab=Artificial%20Neural%20Networks%20Virtual%20Lab
## Observations
- For the given perceptron network, observe that the intial assignment of weights is completely random.
- Observe the number of iterations required for the weights to converge, i.e. to achieve classification for the given case.
- Repeat the experiment for different number of samples per class and observe if there exists a relation between the number of samples per class and iteration steps required to converge .
## Assignment
1. Observe the behaviour of perceptron learning algorithm, for two classes which are (a) linearly separable and (b) linearly inseparable. What is the number of iterations required in each case ?
1. How does the perceptron behave if the two classes are not linearly separable. Assuming that the two classes are overlapping, do the weights converge, or do they oscillate around an optimum value ? In either case, what is the nature of the separating line/hyperplane.
## References
- B. Yegnanarayana, Artificial Neural Networks, New Delhi, India : Prentice-Hall of India, pg. 100, 1999.


